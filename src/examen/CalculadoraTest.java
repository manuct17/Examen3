package examen;

import static org.junit.Assert.*;

import org.junit.Test;

import examen.Calculadora;

public class CalculadoraTest extends Calculadora{

	@Test
	public void testCalculadora() {
		Calculadora calculadora = new Calculadora();
		assertTrue(calculadora.sumar(1,1) == (2));
		assertTrue(calculadora.sumar(3,4) == 7);
		assertTrue(calculadora.sumar(5,45) == 50);
		assertTrue(calculadora.sumar(1,12) == 13);
		assertTrue(calculadora.sumar(4,3) == 7);
		assertTrue(calculadora.sumar(1,4) == 5);
		assertTrue(calculadora.sumar(3,123) == 126);
		assertTrue(calculadora.sumar(1,14) == 15);
		assertTrue(calculadora.sumar(7,5) == 12);
		assertTrue(calculadora.restar(1,1) == 0);
		assertTrue(calculadora.restar(4,3) == 1);
		assertTrue(calculadora.restar(45,5) == 40);
		assertTrue(calculadora.restar(12,1) == 11);
		assertTrue(calculadora.restar(4,3) == 1);
		assertTrue(calculadora.restar(4,1) == 3);
		assertTrue(calculadora.restar(123,21) == 102);
	}
}
